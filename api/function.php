<?php
function deleteMovie($id)
{
    if ($id == -1) {
        $movie_data = array(
            'error' => "1",
            'errorMessage' => "Empty ID"
        );
        return json_encode($movie_data, JSON_UNESCAPED_UNICODE);
    }
    try {
        require "config.php";
        $connection = new mysqli($db_host, $db_user, $db_password, $db_name);
        if ($connection->connect_errno != 0) {
            throw new Exception($connection->connect_error);
        } else {
            $connection->query("SET NAMES utf8");
            
            if ($connection->query("DELETE FROM movies WHERE id='" . $connection->real_escape_string($id) . "'")) {
                $movie_data = array(
                    'error' => "0",
                    'errorMessage' => null
                );
                
                $connection->close();
                return json_encode($movie_data, JSON_UNESCAPED_UNICODE);
            } else {
                throw new Exception($connection->error);
            }
            
            $connection->close();
        }
    }
    catch (Exception $error) {
        echo $error->getMessage();
        $movie_data = array(
            'error' => "1"
        );
        return json_encode($movie_data, JSON_UNESCAPED_UNICODE);
    }
}

function getMovie(int $id)
{
    $error_tab = array(
        'error' => "0"
    );
    
    try {
        require "config.php";
        $connection = new mysqli($db_host, $db_user, $db_password, $db_name);
        if ($connection->connect_errno != 0) {
            throw new Exception($connection->connect_error);
        } else {
            $connection->query("SET NAMES utf8");
            
            if ($result = $connection->query("SELECT * FROM movies WHERE id_user='" . $connection->real_escape_string($id) . "' ORDER BY id DESC")) {
                $movies = array();
                while ($row = $result->fetch_assoc()) {
                    $movies[] = $row;
                }
                $error_tab['movies'] = $movies;
                
                $result->free();
                return json_encode($error_tab, JSON_UNESCAPED_UNICODE);
            } else {
                throw new Exception($connection->error);
            }
            
            
            $connection->close();
        }
    }
    catch (Exception $error) {
        echo $error->getMessage();
        $error_tab['error']        = "1";
        $error_tab['errorMessage'] = $error->getMessage();
        return json_encode($error_tab, JSON_UNESCAPED_UNICODE);
        
    }
}

function login($login, $password)
{
    $error_tab = array(
        'error' => "0"
    );
    
    try {
        require "config.php";
        $connection = new mysqli($db_host, $db_user, $db_password, $db_name);
        ;
        if ($connection->connect_errno != 0) {
            throw new Exception($connection->connect_errno);
        } else {
            if ($result = $connection->query("SELECT * FROM users WHERE login='" . $connection->real_escape_string($login) . "'")) {
                if ($result->num_rows > 0) {
                    $row = $result->fetch_assoc();
                    if (password_verify($password, $row['password'])) {
                        $error_tab['error'] = "0";
                        $error_tab['id']    = $row['id'];
                        $error_tab['login'] = $row['login'];
                        $error_tab['email'] = $row['email'];
                    } else {
                        $error_tab['error']        = "1";
                        $error_tab['errorMessage'] = "Incorrect password";
                    }
                } else {
                    $error_tab['error']        = "2";
                    $error_tab['errorMessage'] = "Incorrect login";
                }
                $result->free_result();
            } else {
                throw new Exception($connection->error);
            }
            $connection->close();
            return json_encode($error_tab, JSON_UNESCAPED_UNICODE);
        }
    }
    catch (Exception $error) {
        $error_tab['error']        = "5";
        $error_tab['errorMessage'] = $error->getMessage();
    }
}

function userRegister($login, $password, $password2, $email)
{
    $error_tab = array(
        'error' => "0",
        'errorMessage' => NULL
    );
    
    $register = true;
    if (!preg_match('/^[a-zA-Z0-9]{3,24}$/D', $login)) {
        $register                  = false;
        $error_tab['error']        = "1";
        $error_tab['errorMessage'] = "Nazwa uzytkownika moze zawierac tylko litery i cyfry (bez polskich znakow). Zakres znaków wynosi od 3 do 24.";
    }
    
    if (strlen($password) < 2 || strlen($password) > 24) {
        $register                  = false;
        $error_tab['error']        = "2";
        $error_tab['errorMessage'] = "Hasło musi zawierac od 3 do 24 znaków.";
    }
    
    if (strcmp($password, $password2) != 0) {
        $register                  = false;
        $error_tab['error']        = "3";
        $error_tab['errorMessage'] = "Podane hasła nie sią identyczne";
    }
    
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $register                  = false;
        $error_tab['error']        = "4";
        $error_tab['errorMessage'] = "Niepoprawny adres email";
    }
    
    if ($register == true) {
        try {
            require "config.php";
            $connection = new mysqli($db_host, $db_user, $db_password, $db_name);
            if ($connection->connect_errno != 0) {
                throw new Exception($connection->connect_error);
            } else {
                $connection->query("SET NAMES utf8");
                
                if ($result = $connection->query("SELECT login FROM users WHERE login='" . $connection->real_escape_string($login) . "'")) {
                    if ($result->num_rows > 0) {
                        $error_tab['error']        = "1";
                        $error_tab['errorMessage'] = "Konto juz istnieje";
                        $result->free_result();
                        $connection->close();
                        return json_encode($error_tab, JSON_UNESCAPED_UNICODE);
                        
                    } else {
                        $query = "INSERT INTO users (login, password, email) VALUES ('" . $connection->real_escape_string($login) . "', '" . password_hash($password, PASSWORD_DEFAULT) . "', '" . $connection->real_escape_string($email) . "')";
                        
                        if (!$connection->query($query)) {
                            throw new Exception($connection->error);
                        } else {
                            $error_tab['error']        = "0";
                            $error_tab['errorMessage'] = NULL;
                        }
                        $result->free_result();
                        $connection->close();
                        return json_encode($error_tab, JSON_UNESCAPED_UNICODE);
                        
                    }
                }
            }
        }
        catch (Exception $error) {
            $error_tab['error']        = "5";
            $error_tab['errorMessage'] = $error->getMessage();
        }
    } else {
        return json_encode($error_tab, JSON_UNESCAPED_UNICODE);
    }
}

function addMovie($id, $name)
{
    $error_tab = array(
        'error' => "0",
        'errorMessage' => NULL
    );
    
    $register = true;
    
    if (strlen($name) < 2 || strlen($name) > 49) {
        $register                  = false;
        $error_tab['error']        = "2";
        $error_tab['errorMessage'] = "Nazwa filmu musi zawierac od 2 do 49 znaków.";
    }
    
    if (empty($id)) {
        $register                  = false;
        $error_tab['error']        = "2";
        $error_tab['errorMessage'] = "Nieprawidłowe id usera";
    }
    
    if ($register == true) {
        try {
            require "config.php";
            $connection = new mysqli($db_host, $db_user, $db_password, $db_name);
            if ($connection->connect_errno != 0) {
                throw new Exception($connection->connect_error);
            } else {
                $connection->query("SET NAMES utf8");
                $query = "INSERT INTO movies (name, id_user) VALUES ('" . $connection->real_escape_string($name) . "', '" . $connection->real_escape_string($id) . "')";
                
                if (!$connection->query($query)) {
                    throw new Exception($connection->error);
                } else {
                    $error_tab['error']        = "0";
                    $error_tab['errorMessage'] = NULL;
                }
                $connection->close();
                return json_encode($error_tab, JSON_UNESCAPED_UNICODE);
                
            }
        }
        catch (Exception $error) {
            $error_tab['error']        = "5";
            $error_tab['errorMessage'] = $error->getMessage();
        }
    } else {
        return json_encode($error_tab, JSON_UNESCAPED_UNICODE);
    }
}