<?php
require_once "function.php";
if (!isset($_POST['id'])) {
    echo deleteMovie(-1);
} else {
    $id = $_POST['id'];
    echo deleteMovie($id);
}
?>