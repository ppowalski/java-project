<?php
require_once "function.php";
if (!isset($_POST['id']) && !isset($_POST['name'])) {
    echo addMovie("", "");
} else {
    $id   = $_POST['id'];
    $name = $_POST['name'];
    echo addMovie($id, $name);
}
?>