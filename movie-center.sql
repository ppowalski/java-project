-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Czas generowania: 12 Lut 2019, 11:40
-- Wersja serwera: 10.1.37-MariaDB-0+deb9u1
-- Wersja PHP: 7.0.33-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `movie-center`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `movies`
--

CREATE TABLE `movies` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `movies`
--

INSERT INTO `movies` (`id`, `name`, `id_user`) VALUES
(40, 'test', 1),
(44, 'sth', 1),
(48, 'asd', 1),
(57, 'jan', 1),
(63, 'tutututu', 1),
(64, 'hffjfd', 1),
(65, 'dgedgshh', 1),
(66, 'tshdhdg', 1),
(67, 'tdgd', 1),
(68, 'atsffg', 1),
(69, 'rehdggfhdr', 1),
(70, 'dhdidhhfj', 1),
(71, 'gfhfty', 1),
(72, 'gswfsjgg', 1),
(73, 'dffdgddffgjjgfdidfgusfftt', 1),
(74, 'daaasssssssssssssss', 1),
(75, 'aaaaaaaaaaaaaaa', 1),
(76, 'dshft', 1),
(77, 'asdhlljfaaaeyjc', 1),
(78, 'tdfgdjffjry', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `email`) VALUES
(1, 'Patryk', '$2y$10$tuLWKSDidkovp8TcwNIXB.MdUgZc/SPsxTFhhIPEJq74GHnfvLbJ2', 'kontakt@patrykpowalski.pl'),
(3, 'Admin', '$2y$10$v6Wyk83jctnJB9IbJRpk5OkUewx5Clt1TpTdGIY2V.Q5XwUD8.JOu', 'admin@movie-center.pl');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `movies`
--
ALTER TABLE `movies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
