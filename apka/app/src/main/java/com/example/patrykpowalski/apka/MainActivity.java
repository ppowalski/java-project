package com.example.patrykpowalski.apka;

import android.content.Intent;
import android.media.Image;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Math.toIntExact;

public class MainActivity extends AppCompatActivity {
    Session session;
    EditText edit;
    TextView username;
    Button logout;
    Button add;
    ListView listView;
    ProgressBar progressBar;
    ImageView delete;
    String[] tab;
    HashMap<String, String> user;
    ArrayList<String> arrayList;
    ArrayAdapter<String> arrayAdapter;

    boolean load = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN); //ukrycie klawiatury i migajacego kursowa na edit
        session = new Session(this);
        session.BackToLogin(); //jezeli nie jest zalogowany ti otworzy logowanie
        user = session.getUserData();

        username = (TextView) findViewById(R.id.textWelcome2);
        add = (Button) findViewById(R.id.addButton);
        logout = (Button) findViewById(R.id.logoutButton);
        listView = (ListView) findViewById(R.id.list_view);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        edit = (EditText) findViewById(R.id.editText);
        edit.clearFocus();

        username.setText(user.get("USERNAME"));


        if(load)
        {
            load = false;
            loadMovie(user.get("ID"));
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                deleteMovie(id);
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMovie(edit.getText().toString());
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.logout();
            }
        });
    }

    public void refreshMovie(String id)
    {
        Cache cache = new DiskBasedCache(getCacheDir(), 1024*1024);
        Network network = new BasicNetwork(new HurlStack());
        final RequestQueue requestQueue = new RequestQueue(cache, network);
        requestQueue.start();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://77.55.213.218/api/index.php?name=movie&id="+id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.optJSONArray("movies");
                            String status = jsonObject.getString("error");

                            if(status.equals("0"))
                            {
                                arrayList.clear();
                                tab = new String[jsonArray.length()];

                                for(int i=0; i<jsonArray.length(); i++)
                                {
                                    JSONObject obj = jsonArray.getJSONObject(i);
                                    tab[i] = obj.getString("id");
                                    arrayList.add(obj.getString("name"));
                                }
                                arrayAdapter.notifyDataSetChanged();

                            }
                            else
                            {
                                Toast.makeText(MainActivity.this, jsonObject.getString("errorMessage"), Toast.LENGTH_SHORT).show();
                            }
                            requestQueue.stop();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(MainActivity.this, "Error: "+e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, "Error: "+error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(stringRequest);
    }

    public void loadMovie(String id)
    {
        listView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        Cache cache = new DiskBasedCache(getCacheDir(), 1024*1024);
        Network network = new BasicNetwork(new HurlStack());
        final RequestQueue requestQueue = new RequestQueue(cache, network);
        requestQueue.start();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://77.55.213.218/api/index.php?name=movie&id="+id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.optJSONArray("movies");
                            String status = jsonObject.getString("error");

                            if(status.equals("0"))
                            {
                                arrayList = new ArrayList<>();
                                tab = new String[jsonArray.length()];

                                for(int i=0; i<jsonArray.length(); i++)
                                {
                                    JSONObject obj = jsonArray.getJSONObject(i);
                                    tab[i] = obj.getString("id");
                                    arrayList.add(obj.getString("name"));
                                }

                                arrayAdapter = new ArrayAdapter<>(MainActivity.this, R.layout.list_white_text, R.id.list_content, arrayList);
                                listView.setAdapter(arrayAdapter);

                                progressBar.setVisibility(View.GONE);
                                listView.setVisibility(View.VISIBLE);
                            }
                            else
                            {
                                Toast.makeText(MainActivity.this, jsonObject.getString("errorMessage"), Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                                listView.setVisibility(View.VISIBLE);
                            }
                            requestQueue.stop();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(MainActivity.this, "Error: "+e.toString(), Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.GONE);
                            listView.setVisibility(View.VISIBLE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, "Error: "+error.toString(), Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);
                        listView.setVisibility(View.VISIBLE);
                    }
                }
        );
        requestQueue.add(stringRequest);
    }

    public void addMovie(final String text)
    {
        Cache cache = new DiskBasedCache(getCacheDir(), 1024*1024);
        Network network = new BasicNetwork(new HurlStack());
        final RequestQueue requestQueue = new RequestQueue(cache, network);
        requestQueue.start();

        final StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://77.55.213.218/api/add.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("error");

                            if(status.equals("0"))
                            {
                                refreshMovie(user.get("ID"));
                                edit.onEditorAction(EditorInfo.IME_ACTION_DONE);
                                edit.setText("");
                            }
                            else
                            {
                                Toast.makeText(MainActivity.this, jsonObject.getString("errorMessage"), Toast.LENGTH_SHORT).show();
                                edit.clearFocus();
                                edit.setText("");
                            }
                            requestQueue.stop();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(MainActivity.this, "Error: "+e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, "Error: "+error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", text);
                params.put("id", user.get("ID"));
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    public void deleteMovie(long id)
    {
        final int delete_id = (int) id;

        Cache cache = new DiskBasedCache(getCacheDir(), 1024*1024);
        Network network = new BasicNetwork(new HurlStack());
        final RequestQueue requestQueue = new RequestQueue(cache, network);
        requestQueue.start();

        final StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://77.55.213.218/api/delete.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("error");

                            if(status.equals("0"))
                            {
                                refreshMovie(user.get("ID"));
                            }
                            else
                            {
                                Toast.makeText(MainActivity.this, jsonObject.getString("errorMessage"), Toast.LENGTH_SHORT).show();
                            }
                            requestQueue.stop();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(MainActivity.this, "Error: "+e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, "Error: "+error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
            Map<String, String> params = new HashMap<>();
            params.put("id", tab[delete_id]);
            return params;
        }
        };
        requestQueue.add(stringRequest);
    }

    public void onBackPressed() {
        if(!session.isLogin())
        {
            this.finish();
        }
        else
        {
            this.moveTaskToBack(true);
        }
    }
}
