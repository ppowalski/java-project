package com.example.patrykpowalski.apka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    EditText login;
    EditText password;
    EditText password2;
    EditText email;
    Button register;
    ProgressBar loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        login = (EditText) findViewById(R.id.inputLogin);
        password = (EditText) findViewById(R.id.inputPassword);
        password2 = (EditText) findViewById(R.id.inputPassword2);
        email = (EditText) findViewById(R.id.inputEmail);
        register = (Button) findViewById(R.id.registerButton);
        loading = (ProgressBar) findViewById(R.id.registerLoading);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });
    }

    protected void openLoginActivity()
    {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void registerUser()
    {
        register.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);

        final String login = this.login.getText().toString();
        final String password = this.password.getText().toString();
        final String password2 = this.password2.getText().toString();
        final String email = this.email.getText().toString();

        Cache cache = new DiskBasedCache(getCacheDir(), 1024*1024);
        Network network = new BasicNetwork(new HurlStack());
        final RequestQueue requestQueue = new RequestQueue(cache, network);
        requestQueue.start();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://77.55.213.218/api/register.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("error");
                            if(status.equals("0"))
                            {
                                register.setVisibility(View.VISIBLE);
                                loading.setVisibility(View.GONE);
                                openLoginActivity();
                            }
                            else
                            {
                                Toast.makeText(RegisterActivity.this, jsonObject.getString("errorMessage"), Toast.LENGTH_SHORT).show();
                                register.setVisibility(View.VISIBLE);
                                loading.setVisibility(View.GONE);
                            }
                            requestQueue.stop();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(RegisterActivity.this, "Error: "+e.toString(), Toast.LENGTH_SHORT).show();
                            register.setVisibility(View.VISIBLE);
                            loading.setVisibility(View.GONE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(RegisterActivity.this, "Error: "+error.toString(), Toast.LENGTH_SHORT).show();
                        register.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                    }
                }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("login", login);
                params.put("password", password);
                params.put("password2", password2);
                params.put("email", email);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
