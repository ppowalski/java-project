package com.example.patrykpowalski.apka;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.HashMap;

public class Session {
    SharedPreferences sharedPreferences;

    public SharedPreferences.Editor editor;
    public Context context;
    int PRIVATE_MODE = 0;

    public Session(Context context)
    {
        this.context = context;
        sharedPreferences = context.getSharedPreferences("LOGIN", PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void CreateSession(String id, String login)
    {
        editor.putBoolean("IS_LOGIN", true);
        editor.putString("ID", id);
        editor.putString("USERNAME", login);
        editor.apply();
    }

    public void BackToLogin()
    {
        if(!isLogin())
        {
            Intent intent = new Intent(context, LoginActivity.class);
            context.startActivity(intent);
        }
    }

    public boolean isLogin()
    {
        return sharedPreferences.getBoolean("IS_LOGIN", false); //jezeli nie ma ustawionego to domyslnie jest false
    }

    public HashMap<String, String> getUserData()
    {
        HashMap<String, String> user = new HashMap<>();
        user.put("ID", sharedPreferences.getString("ID", null));
        user.put("USERNAME", sharedPreferences.getString("USERNAME", null));
        return user;
    }

    public void logout()
    {
        editor.clear();
        editor.commit();
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }
}
