package com.example.patrykpowalski.apka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    Session session;
    EditText login;
    EditText password;
    Button signIn;
    TextView createAccout;
    ProgressBar loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        session = new Session(this);

        login = (EditText) findViewById(R.id.inputLogin1);
        password = (EditText) findViewById(R.id.inputPassword1);
        signIn = (Button) findViewById(R.id.loginButton);
        createAccout = (TextView) findViewById(R.id.textRegister);
        loading = (ProgressBar) findViewById(R.id.loading);

        signIn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                loginUser();
            }
        });

        createAccout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                openRegisterActivity();
            }
        });
    }

    protected void openRegisterActivity()
    {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    protected void openMainActivity()
    {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    protected void loginUser()
    {
        signIn.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);

        final String login = this.login.getText().toString();
        final String password = this.password.getText().toString();

        Cache cache = new DiskBasedCache(getCacheDir(), 1024*1024);
        Network network = new BasicNetwork(new HurlStack());
        final RequestQueue requestQueue = new RequestQueue(cache, network);
        requestQueue.start();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://77.55.213.218/api/login.php",
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("error");

                        if(status.equals("0"))
                        {
                            String user_id = jsonObject.getString("id");
                            String username = jsonObject.getString("login");
                            session.CreateSession(user_id, username);
                            signIn.setVisibility(View.VISIBLE);
                            loading.setVisibility(View.GONE);
                            openMainActivity();
                        }
                        else
                        {
                            Toast.makeText(LoginActivity.this, jsonObject.getString("errorMessage"), Toast.LENGTH_SHORT).show();
                            signIn.setVisibility(View.VISIBLE);
                            loading.setVisibility(View.GONE);
                        }
                        requestQueue.stop();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(LoginActivity.this, "Error: "+e.toString(), Toast.LENGTH_SHORT).show();
                        signIn.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(LoginActivity.this, "Error: "+error.toString(), Toast.LENGTH_SHORT).show();
                    signIn.setVisibility(View.VISIBLE);
                    loading.setVisibility(View.GONE);
                }
            }
        )
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("login", login);
                params.put("password", password);
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}
